export class Hero {
    private name: string;
    private health: number;
    private maxHealth: number = 100;
    
    constructor(name: string, health: number = 100) {
        this.name = name;
        if(health < this.maxHealth) {
            this.health = health;
        } else {
            this.health = this.maxHealth;
        }
    }
    
    attacked(attackValue: number) {
        if (attackValue > this.health) {
            console.log(`${this.name} necesita un buen descanso`);
        } else {
            this.health -= attackValue;
            console.log(`Héroe atacado: ${attackValue} -- ${this.health}`);
        }
    }

    heal(healValue: number) {
        if (this.health + healValue > this.maxHealth) {
          console.log(`${this.name} Tiene su salud al completo... ${this.maxHealth}`);
        } else {
          this.health += healValue;
          console.log(`${this.name} ha recuperado salud y está en: ${this.health}`);
        }
      }
}