export class Villain {
    private name: string;
    private health: number;
    private maxHealth: number = 100;

    constructor(name: string, health: number = 100) {
      this.name = name;
      if (health < this.maxHealth) {
        this.health = health;
      } else {
        this.health = this.maxHealth;
      }
    }
  
    rampage() {
      if (this.health <= 10) {
        this.health = this.maxHealth * 0.90
        console.log(`${this.name} Se desmadra y restaura su salud a ${this.health}`);
      } else {
        console.log(`${this.name} es un poco débil.`);
      }
    }
  
    attacked(attackValue: number) {
      if (attackValue > this.health) {
        console.log(`${this.name} necesita un buen descanso.`);
      } else {
        this.health -= attackValue;
        console.log(`Villano atacado: ${attackValue} -- ${this.health}`);
      }
    }
  
    heal(healValue: number) {
      if (this.health + healValue > this.maxHealth) {
        console.log(`${this.name} Tiene su salud al completo... ${this.maxHealth}`);
      } else {
        this.health += healValue;
        console.log(`${this.name} ha recuperado salud y está en: ${this.health}`);
      }
    }
  }