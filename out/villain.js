"use strict";
exports.__esModule = true;
var Villain = /** @class */ (function () {
    function Villain(name, health) {
        if (health === void 0) { health = 100; }
        this.maxHealth = 100;
        this.name = name;
        if (health < this.maxHealth) {
            this.health = health;
        }
        else {
            this.health = this.maxHealth;
        }
    }
    Villain.prototype.rampage = function () {
        if (this.health <= 10) {
            this.health = this.maxHealth * 0.90;
            console.log(this.name + " Se desmadra y restaura su salud a " + this.health);
        }
        else {
            console.log(this.name + " es un poco d\u00E9bil.");
        }
    };
    Villain.prototype.attacked = function (attackValue) {
        if (attackValue > this.health) {
            console.log(this.name + " necesita un buen descanso.");
        }
        else {
            this.health -= attackValue;
            console.log("Villano atacado: " + attackValue + " -- " + this.health);
        }
    };
    Villain.prototype.heal = function (healValue) {
        if (this.health + healValue > this.maxHealth) {
            console.log(this.name + " Tiene su salud al completo... " + this.maxHealth);
        }
        else {
            this.health += healValue;
            console.log(this.name + " ha recuperado salud y est\u00E1 en: " + this.health);
        }
    };
    return Villain;
}());
exports.Villain = Villain;
