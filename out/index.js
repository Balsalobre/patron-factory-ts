"use strict";
exports.__esModule = true;
// app.ts
var superherofactory_1 = require("./superherofactory");
var superheroFactory = new superherofactory_1.SuperHeroFactory();
var batman = superheroFactory.createSuperHero({ name: 'Batman', type: "hero" });
var joker = superheroFactory.createSuperHero({ name: 'Joker', health: 50, type: "villain" });
batman.attacked(40); // Hero attacked: 40 -- 60
joker.attacked(40); // Villain attacked: 40 -- 10
joker.rampage(); // Joker restored health to 90
