"use strict";
exports.__esModule = true;
var Hero = /** @class */ (function () {
    function Hero(name, health) {
        if (health === void 0) { health = 100; }
        this.maxHealth = 100;
        this.name = name;
        if (health < this.maxHealth) {
            this.health = health;
        }
        else {
            this.health = this.maxHealth;
        }
    }
    Hero.prototype.attacked = function (attackValue) {
        if (attackValue > this.health) {
            console.log(this.name + " necesita un buen descanso");
        }
        else {
            this.health -= attackValue;
            console.log("H\u00E9roe atacado: " + attackValue + " -- " + this.health);
        }
    };
    Hero.prototype.heal = function (healValue) {
        if (this.health + healValue > this.maxHealth) {
            console.log(this.name + " Tiene su salud al completo... " + this.maxHealth);
        }
        else {
            this.health += healValue;
            console.log(this.name + " ha recuperado salud y est\u00E1 en: " + this.health);
        }
    };
    return Hero;
}());
exports.Hero = Hero;
